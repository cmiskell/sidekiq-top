#!/usr/bin/env ruby

require 'rb-inotify'
require 'curses'
require 'json'
require 'sys/proctable'
require 'optparse'
require 'date'

Options = Struct.new(:filename, :replay, :start_from)

options = Options.new('/var/log/gitlab/sidekiq-cluster/current', false, nil)

OptionParser.new do |opts|
  opts.on('-fFILENAME' 'The log file to read') do |f|
    options.filename = f
  end
  opts.on('-sSTARTFROM', 'Start from date-time') do |s|
    options.start_from = DateTime.parse(s).to_time
  end
  opts.on('-r', 'Replay rather than read live') do |r|
    options.replay = r
  end
end.parse!

state = {
  'concurrency' => nil,
  'sidekiq_processes' => Hash.new { |hash,key| hash[key] = Hash.new },
  'jobs_by_name' => Hash.new do
    |hash,key| hash[key] = {
      'active' => 0,
      'total_count' => 0,
      'total_duration' => 0,
      'total_cpu_s' => 0,
      'last_seen' => Time.now
    }
  end,
  'deduplicated' => Hash.new { |hash,key| hash[key] = 0 },
  'status_line' => 'sidekiq-top',
  'last_seen_date' => "",
}

def find_processes(state)
  sidekiq_processes = state['sidekiq_processes']
  cluster_regex = Regexp.new('^ruby /opt/gitlab/embedded/service/gitlab-rails/bin/sidekiq-cluster .*-m +([0-9]+ )')
  Sys::ProcTable.ps do |p|
    sidekiq_processes[p.pid] if p.cmdline.match(/^sidekiq [0-9.]+ queues:/)
    state['concurrency'] = $1.to_i if p.cmdline.match(cluster_regex)
  end
end

def record_data(state, data)
  job_name = data["class"]
  job_name = data["wrapped"] if job_name == "ActiveJob::QueueAdapters::SidekiqAdapter::JobWrapper"
  status = data["job_status"]
  job_id = data["jid"]
  pid = data["pid"]

  # Seen on e.g. ActiveRecord::QueryCanceled *after* we've
  # seen the 'fail' log.  Can just ignore this
  return if !pid

  process = state['sidekiq_processes'][pid]
  jobs_by_name = state['jobs_by_name']
  deduplicated = state['deduplicated']
  state['last_seen_date'] = data['time']

  case status
  when "start"
    process[job_id] = job_name
    job = jobs_by_name[job_name]
    job['active'] += 1
    job['total_count'] += 1
    job['last_seen'] = Time.now
  when "done", "fail"
    if process.delete(job_id) != nil
      job = jobs_by_name[job_name]
      count = job['active']
      count -= 1
      # possibly not required, now we check if we actually deleted the job
      count = 0 if count < 0
      job['active'] = count
      job['total_duration'] += data['duration_s']
      job['total_cpu_s'] += data['cpu_s']
      job['last_seen'] = Time.now
    end
  when "deduplicated"
    deduplicated[job_name] += 1
  else
    state['status_line'] = "What to do with status #{status}?"
  end
end

def setup_display()
  Curses.init_screen
  Curses.crmode
  Curses.curs_set(0)
end

def display_state(state)
  sidekiq_processes = state['sidekiq_processes']
  jobs_by_name = state['jobs_by_name']
  deduplicated = state['deduplicated']

  pid_count = sidekiq_processes.length
  Curses.setpos(0, 0)
  Curses.attrset(Curses::A_BOLD)
  Curses.addstr("PID   Running jobs")
  Curses.attrset(Curses::A_NORMAL)
  sidekiq_processes.keys.sort.each_with_index do |pid, index|
    Curses.setpos(index + 1, 0)
    len = sidekiq_processes[pid].length
    Curses.addstr(sprintf("%-6s%s%s", pid, "█"*len, "░"*(state['concurrency'] - len)))
    Curses.clrtoeol()
  end

  Curses.attrset(Curses::A_BOLD)
  Curses.setpos(pid_count + 1, 0)
  Curses.addstr(   "#{" "*60}              | Total (s)           | Avg (s)")
  Curses.setpos(pid_count + 2, 0)
  Curses.addstr("Job#{" "*57}Running   |   |  Cnt| CPU  |Duration| CPU  |Duration")
  Curses.attrset(Curses::A_NORMAL)

  num_jobs = jobs_by_name.length
  # 4 = processes header, line space, job header, and status line at the bottom
  remove_count = num_jobs - (Curses.lines - 4 - sidekiq_processes.length)

  while remove_count > 0
    oldest_job = jobs_by_name.keys.min do |a,b|
      jobs_by_name[a]['last_seen'] <=> jobs_by_name[b]['last_seen']
    end
    jobs_by_name.delete(oldest_job)
    remove_count -= 1
  end

  jobs_by_name.keys.sort.each_with_index do |job_name, index|
    Curses.setpos(index + pid_count + 3, 0)
    job = jobs_by_name[job_name]
    active = job['active']
    count = job['total_count']
    cpu_s = job['total_cpu_s']
    duration =job['total_duration']
    Curses.addstr(sprintf("%-60.60s%-10.10s|%3.3s|%5.5s|%6.1f|%8.2f|%6.2f|%8.2f", job_name, "█"*active, active.to_s, count, cpu_s, duration, cpu_s/count, duration/count))
    Curses.clrtoeol()
  end

  if state['status_line']
    Curses.setpos(Curses.lines-1, 0)
    Curses.clrtoeol()
    Curses.addstr(state['status_line'].to_s)
  end

  datestr = state['last_seen_date'].to_s
  Curses.setpos(Curses.lines-1, Curses.cols - datestr.length)
  Curses.addstr(datestr)

  Curses.refresh
end

setup_display()
find_processes(state)
display_state(state)

trap("INT") do
  exit
end

log = open(options.filename)
if options.replay
  if options.start_from
    # TODO: use seek to binary search the file
    count = 0
    last_log_time = Time.at(0)
    while last_log_time < options.start_from
      line = log.gets
      abort "Reached end of file without finding start time" unless line
      begin
        new_data = JSON.parse(line)
        last_log_time = DateTime.parse(new_data['time']).to_time
        count += 1
        if (count % 1000) == 0
          state['last_seen_date'] = "Scrolling to start time: #{last_log_time}"
          display_state(state)
        end
      rescue JSON::ParserError
        # Swallow/ignore
      end
    end
  else
    # As odd as this look, it avoids complexity later, and leads to an initial 0 sleep
    last_log_time = Time.now
  end

  while log.gets
    # TODO: consider trying to read (nonblocking) keypresses to scroll through time (pause, rewind, fast-forward)
    begin
      new_data = JSON.parse($_)
      this_log_time = DateTime.parse(new_data['time']).to_time
      record_data(state, new_data)
    rescue JSON::ParserError => e
      state['status_line'] = e
    end
    timediff = this_log_time - last_log_time
    sleep_time = timediff > 0 ? timediff : 0
    sleep(sleep_time) if last_log_time
    display_state(state)
    last_log_time = this_log_time
  end
else
  #Live stream
  log.seek(0, IO::SEEK_END)     # rewinds file to the end
  queue = INotify::Notifier.new
  # Sometimes we get notified with a partial line available, so keep that partial in a buffer.
  # readline throws on EOF, which would make this ugly (exceptions for normal workflow)
  buffer = ""
  queue.watch(options.filename, :modify, :close_write) do |event|
    if event.flags.include?(:modify)
      newdata = log.read
      lines = newdata.encode('UTF-8', :invalid => :replace).split("\n")
      # prepend any previous buffer (left over from the last read) to the first line
      lines[0] = "#{buffer}#{lines[0]}"
      if (newdata.end_with?("\n"))
        buffer = ""
      else
        # Save the last 'line' (it had no newline at the end) for later
        buffer = lines.pop
      end

      lines.each do |line|
        begin
          new_data = JSON.parse(line)
          record_data(state, new_data)
        rescue JSON::ParserError => e
          state['status_line'] = e
        end
        display_state(state)
      end
    elsif event.flags.include?(:close_write)
      # File has been closed for writing, presumably rotated.  Re-open it
      log.close()
      log = open(filename)
      # TODO: Looks like we need to do more to kickstart the inotify listener/queue
      # Do not seek; next modify event will read from the start
    end
  end
  queue.run
end
