u:Gem::Specification�[I"
3.0.6:ETi	I"sys-proctable; TU:Gem::Version[I"
1.2.3; TIu:	Time 
�    :	zoneI"UTC; FI"9An interface for providing process table information; TU:Gem::Requirement[[[I">=; TU;[I"0; TU;	[[[I">=; TU;[I"0; TI"	ruby; T[o:Gem::Dependency
:
@nameI"ffi; T:@requirementU;	[[[I">=; TU;[I"0; T:
@type:runtime:@prereleaseF:@version_requirementsU;	[[[I">=; TU;[I"0; To;

;I"
rspec; T;U;	[[[I">=; TU;[I"0; T;:development;F;U;	[[[I">=; TU;[I"0; To;

;I"	rake; T;U;	[[[I">=; TU;[I"0; T;;;F;U;	[[[I">=; TU;[I"0; TI" ; TI"djberg96@gmail.com; T[I"Daniel J. Berger; TI"0    The sys-proctable library provides an interface for gathering information
    about processes on your system, i.e. the process table. Most major
    platforms are supported and, while different platforms may return
    different information, the external interface is identical across
    platforms.
; TI"-http://github.com/djberg96/sys-proctable; TT@[I"Apache-2.0; T{I"homepage_uri; TI".https://github.com/djberg96/sys-proctable; TI"bug_tracker_uri; TI"5https://github.com/djberg96/sys-proctable/issues; TI"changelog_uri; TI"Bhttps://github.com/djberg96/sys-proctable/blob/master/CHANGES; TI"documentation_uri; TI"3https://github.com/djberg96/sys-proctable/wiki; TI"source_code_uri; TI".https://github.com/djberg96/sys-proctable; TI"wiki_uri; TI"3https://github.com/djberg96/sys-proctable/wiki; T