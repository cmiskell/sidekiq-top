# What

A command-line curses-based tool for watching the running state of a sidekiq-cluster

Very young

# Why
It's fine to see all our high level data in Prometheus and Grafana, and that's the only way to deal with the entire system. However I've been feeling a lack of that gut-level visceral instinctive feel for what happens on a node at a fine-grained level, much as once you've seen a few strace logs or packet captures you start to get a feel for a system.

It's one thing to know that e.g. RepositoryImportWorker takes a number of seconds to run, quite another to see a few dozen get picked up on a node and keep a number of workers busy for an extended period.  Or see the often uneven distribution of work across workers, or to watch a large mail-out event flow through.

# How
Ruby code reading the realtime sidekiq-cluster logs (JSON) and summarizing to a curses-based screen output. 

# Bugs
Likely many, although it does basically work.

Tries, but fails, to re-open the log file when it is rotated.  Not sure why yet, needs work (see TODO in code).

The code is ugly, and not particularly idiomatic ruby.  The core loop in particular is in desparate need of refactoring, but it's only just out of the "can I even do this?" phase.  It will be cleaned up in due course, as maintainability becomes an issue 

# Deploy
Requires some gems that are not part of omnibus, particularly 'curses' which also requires an extension library be compiled and omnibus does not include/require a compiler or the ruby headers.  Therefore against usual best-practice, the required gems (over and above what's in omnibus) are included inline in this repo (.gem directory).  To deploy, copy the entire repo to a node with gitlab omnibus installed.  Perhaps we might add these gems to the omnibus deploy one day, but until then, needs must.

The included `sidekiq-top` wrapper script takes care of setting up the gem paths.

# Running
`sudo sudo -u git /path/to/sidekiq-top`

It needs to read `/var/log/gitlab/sidekiq-cluster/current` where that directory is only readable by `git`.  Simple sudo to root also works, but runs at a higher privilege than necessary.

# TODO
* Pause display (but not data capture)
* Sort by other fields
* More stats (percentiles?  min/max?)  Probably requires CLI or keypress options to change modes

* Extension: Read historical logs e.g. a --from command-line argument supplying a timestamp to start from.  Playback from that time, ideally with a pause/forward/back time-scrubber so we can go back and watch what happend.

* Super-extension: Read historical logs from Elasticsearch, so we can get a view across the entire fleet.  Probably more suited to a web interface, but as for the previous extension, gives the ability to scrub through time and go back to watch historical incidents.
